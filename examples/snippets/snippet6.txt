//tiers can be forced by the programmer using the syntax below.
{
    int<1> max;
    int<0> res;
    boolean<1> condition;

    max = 10;
    res = 1;

    condition = max >= 0;

    while(condition) {
        max = max - 1;

        if(max > 0) {
            res = res + 1;
        }
        else {
            res = res - 1;
        }

        condition = max >= 0;
    }
}