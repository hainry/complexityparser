/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package lib;

import java.util.ArrayList;

/**
 * A simple abstract class to make it easier to update the GUI.
 */
public abstract class Observable {

    private ArrayList<Observer> list;

    /**
     * Every observer will be notified when the update method is called.
     */
    public Observable() {
        list = new ArrayList<>();
    }

    /**
     * Adds an observer to the list.
     *
     * @param o - the observer to add.
     */
    public void addObserver(Observer o) {
        list.add(o);
    }

    /**
     * Removes a component from the list.
     *
     * @param o - the observer to remove.
     */
    public void removeObserver(Observer o) {
        list.remove(o);
    }

    /**
     * Notifies the observers so that they can be updated.
     */
    public void update() {
        for (Observer o : list) {
            o.update(this);
        }
    }
}
