/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package lib;

import java.util.Arrays;

/**
 * A basic 'bitfield' used as mask for the compatibility check of operators.
 */
public class Mask {

    private boolean[] array;

    public Mask(int size) {
        array = new boolean[size];
        Arrays.fill(array, false);
    }

    /**
     *
     * @param i - an integer.
     * @return the value at index i (no checks are done on the index).
     */
    public boolean get(int i) {
        return array[i];
    }

    /**
     * Sets the value at index i (no checks are done on the index).
     *
     * @param i - an integer.
     * @param v - a boolean.
     */
    public void set(int i, boolean v) {
        array[i] = v;
    }

    /**
     *
     * @return the size of the mask.
     */
    public int size() {
        return array.length;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("Mask[");
        for (int i = 0; i < size(); i++) {
            if (get(i)) {
                str.append("1");
            } else {
                str.append("0");
            }

            if (i < size() - 1) {
                str.append(", ");
            }
        }
        str.append("]");
        return str.toString();
    }
}
