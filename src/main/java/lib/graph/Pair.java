/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package lib.graph;

import java.util.Iterator;

/**
 * A pair associating a node number to an iterator corresponding to existing
 * connections used only during the DFS performed inside the Graph class.
 */
public class Pair {

    private int first;
    private Iterator<Integer> second;

    public Pair(int first, Iterator<Integer> second) {
        this.first = first;
        this.second = second;
    }

    public int getFirst() {
        return first;
    }

    public boolean hasNext() {
        return second.hasNext();
    }

    public int next() {
        return second.next();
    }
}
