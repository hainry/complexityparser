/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package lib.graph;

import org.antlr.v4.runtime.tree.ParseTree;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * A node of the graph (a method in the parse tree).
 */
public class Node implements Iterable<Integer> {

    private Graph g;
    private String name;
    private ParseTree ctx;
    private ArrayList<Integer> dst;
    private boolean recursive;

    public Node(Graph g, String name, ParseTree ctx) {
        this.name = name;
        this.ctx = ctx;
        dst = new ArrayList<>();
        this.g = g;
        recursive = false;
    }

    public Node(Graph g, Node n) {
        this.g = g;
        name = n.name;
        ctx = n.ctx;
        recursive = n.recursive;
        dst = new ArrayList<>();
    }

    /**
     * Adds an arrow to the node with the given index.
     *
     * @param i - a node index.
     */
    public void add(int i) {
        if (!dst.contains(i)) {
            dst.add(i);
        }
    }

    /**
     *
     * @return the (ANTLR) parse tree of the node.
     */
    public ParseTree getCtx() {
        return ctx;
    }

    /**
     * Sets the ANTLR parse tree node
     *
     * @param ctx
     */
    public void setCtx(ParseTree ctx) {
        this.ctx = ctx;
    }

    /**
     *
     * @return true if the method corresponding to the current node is
     * recursive.
     */
    public boolean isRecursive() {
        return recursive;
    }

    /**
     * Sets the isrecursive attribute to the boolean parameter value.
     *
     * @param recursive
     */
    public void setRecursive(boolean recursive) {
        this.recursive = recursive;
    }

    /**
     * Sorts the arrows of this node using the order given in the parameter
     * list.
     *
     * @param order - an arraylist of node names.
     */
    public void sort(ArrayList<String> order) {
        ArrayList<Integer> res = new ArrayList<>(dst.size());
        for (String i : order) {
            int no = g.getNo(i);
            if (dst.contains(no)) {
                res.add(no);
            }
        }
        dst = res;
    }

    /**
     *
     * @return the name of the node (i.e. the namespace of the method).
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return an iterator over the existing arrows of this node.
     */
    public Iterator<Integer> iterator() {
        return dst.iterator();
    }

    /**
     *
     * @return the number of outgoing arrows.
     */
    public int size() {
        return dst.size();
    }

    @Override
    public String toString() {
        return "Node { "
                + "name = '" + name + '\''
                + ", recursive = " + recursive
                + " }";
    }
}
