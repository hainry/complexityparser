/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser;

import complexityparser.analyse.antlr.JavaLexer;
import complexityparser.analyse.antlr.JavaParser;
import complexityparser.listeners.CallGraphBuilder;
import complexityparser.listeners.TOSBuilder;
import complexityparser.listeners.TypingListener;
import complexityparser.types.env.TypingEnv;
import complexityparser.types.Tier;
import complexityparser.typingVisitors.FinalPass;
import complexityparser.typingVisitors.FirstPass;
import complexityparser.typingVisitors.SecondPass;
import lib.Logger;
import lib.Observable;
import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

/**
 * This class contains the code to be analysed by ANTLR and the resulting parse,
 * lexer, and parse tree.
 */
public class Model extends Observable {

    private String code;
    private JavaLexer lexer;
    private CommonTokenStream token;
    private JavaParser parser;
    private ParserRuleContext context;
    private String output;
    private String error;
    private Tier result;
    private boolean syntaxError = false;

    public Model() {
        //The default Java program is below and in default.java.
        String defaultCode = "class Exe{  void main() {\n #init \n #init \n }}";
        Path p = new File("default.java").toPath();
        try {
            byte[] res = Files.readAllBytes(p);
            defaultCode = new String(res, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        setCode(defaultCode);
    }

    public String getOutput() {
        return output;
    }

    public String getError() {
        return error;
    }

    public Tier getResult() {
        return result;
    }

    public boolean getSyntaxError() {
        return syntaxError;
    }

    public void setSyntaxError(boolean syntaxError) {
        this.syntaxError = syntaxError;
    }

    /**
     *
     * @return a java swing component representing a graphical view of the ANTLR
     * generated parse tree.
     */
    public TreeViewer getTreeComponent() {
        TreeViewer viewer = new TreeViewer(Arrays.asList(parser.getRuleNames()), context);
        // The scale of the ANTLR tree.
        viewer.setScale(0.8);
        viewer.setTextColor(viewer.getForeground());
        return viewer;
    }

    /**
     * Changes the String code that has to be analysed and performs the code
     * analysis (It generates a new ANTLR parse tree).
     *
     * @param code - a string of code.
     */
    public void setCode(String code) {
        TypingEnv.getInstance().clear();
        this.code = code;
        CharStream input = CharStreams.fromString(code);
        lexer = new JavaLexer(input);
        token = new CommonTokenStream(lexer);
        parser = new JavaParser(token);
        //Changes to compilationUnit to use the entire Java grammar.
        context = parser.compilationUnit();
        //Builds the table of symbols (TOS).
        TOSBuilder tosBuilder = new TOSBuilder();
        ParseTreeWalker.DEFAULT.walk(tosBuilder, context);
        //Identifies the types of every expression in the generated parse tree.
        TypingListener typingListener = new TypingListener(tosBuilder.getBlockNumbers());
        ParseTreeWalker.DEFAULT.walk(typingListener, context);
        //Creates the method call graph.
        CallGraphBuilder callGraphBuilder = new CallGraphBuilder(tosBuilder, typingListener);
        ParseTreeWalker.DEFAULT.walk(callGraphBuilder, context);

        //optional: Writes the call graph to a *.dot file
        //which can be compiled to an image using the dot command.
        //callGraphBuilder.getGraph().toFile("res.dot");
        //optional: Prints the strongly connected components to stdout.
        //System.out.println(callGraphBuilder.getGraph().getComponentsString());
        //Creates new outputs to redirect stdout and stderr to the interface instead of stdout.
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Logger.setOut(new PrintStream(out));
        ByteArrayOutputStream err = new ByteArrayOutputStream();
        Logger.setErr(new PrintStream(err));
        //Executes the first pass.
        FirstPass firstPass = new FirstPass(callGraphBuilder.getGraph(), tosBuilder, typingListener, token);
        firstPass.visit();
        //Executes the second pass.
        SecondPass secondPass = new SecondPass(firstPass, callGraphBuilder.getGraph(), tosBuilder, typingListener, token);
        secondPass.visit();
        /*
        Executes the last pass. Note that this pass call an overload visit method. 
        This is very important as it will not work otherwise
        first and second pass need to be called with visit()
        final pass needs to be called with visit(ParseTree).
         */
        FinalPass finalPass = new FinalPass(secondPass, tosBuilder, typingListener, token);
        finalPass.visit(context);
        Logger.setOut(System.out);
        Logger.setErr(System.err);
        output = out.toString();
        error = err.toString();
        result = finalPass.getResult();
        System.out.println("---------------------------------------");
        //optional: Prints the operators list and the TOS to stdout.
        //usually a good idea for debugging and testing
        System.out.println(TypingEnv.getInstance());
        //System.out.println(TOS.getInstance());
        //calls update on the Observer objects
        update();
    }

    /**
     *
     * @return the code that has been analysed by ANTLR.
     */
    public String getCode() {
        return code;
    }
}