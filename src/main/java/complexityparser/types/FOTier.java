/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.types;

import lib.Mask;
import java.util.Arrays;
import java.util.Objects;

public class FOTier {

    private Tier[] in;
    private Tier out;
    private Tier env;

    /**
     * Class for encoding the First Order Tier of operators, constructors, and
     * methods.
     *
     * @param out - the output tier.
     * @param in - the input tiers.
     */
    public FOTier(Tier out, Tier... in) {
        this.in = in;
        this.out = out;
    }

    public FOTier(Tier out, Tier env, Tier... in) {
        this.in = in;
        this.out = out;
        this.env = env;
    }

    public FOTier() {
    }

    /**
     *
     * @return the input tiers.
     */
    public Tier[] getIn() {
        return in;
    }

    /**
     *
     * @return the output tier.
     */
    public Tier getOut() {
        return out;
    }

    /**
     * Sets the output tier.
     *
     * @param out - a tier.
     */
    public void setOut(Tier out) {
        this.out = out;
    }

    /**
     *
     * @return the environment tier (unused for operators).
     */
    public Tier getEnv() {
        return env;
    }

    /**
     * Sets the environment tier.
     *
     * @param env - a tier.
     */
    public void setEnv(Tier env) {
        this.env = env;
    }

    /**
     *
     * @param in - input tiers.
     * @return true if the parameter tiers match the input tiers.
     */
    public boolean contains(Tier... in) {
        return Arrays.equals(this.in, in);
    }

    /**
     *
     * @param m - a mask representing 'compatible' values (true) and 'equal'
     * values (false).
     * @param in - input tiers.
     * @return true if the operator input tiers are compatible with the
     * parameter tiers and respect the parameter mask.
     */
    public boolean isCompatibleWith(Mask m, Tier... in) {
        if (in.length != this.in.length) {
            return false;
        }
        for (int i = 0; i < in.length; i++) {
            if (m.get(i)) {
                //If compatibility is possible, then the given input needs to be 
                //the maximum of the input tiers and the operator input tiers
                //(this applies mostly to primitive types).
                if (Tier.max(in[i], this.in[i]) != in[i]) {
                    return false;
                }
            } else {
                //otherwise the values must be equal (this applies mostly to reference types).
                if (in[i] != this.in[i]) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     *
     * @return the number of input tiers (arity) of this FOTier.
     */
    public int size() {
        return in.length;
    }

    @Override
    public String toString() {
        return Arrays.toString(in) + " > " + "(" + out + ", " + env + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FOTier output = (FOTier) o;
        return out == output.out
                && env == output.env;
    }

    @Override
    public int hashCode() {
        return Objects.hash(out, env);
    }

}
