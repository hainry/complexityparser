/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.types;

/**
 * A simple enum used to represent tiers (O,1, None).
 */
public enum Tier {
    None,
    T0,
    T1,;

    /**
     *
     * @param n - an integer.
     * @return a tier created from the specified integer.
     */
    public static Tier toTier(int n) {
        switch (n) {
            case 0:
                return T0;
            case 1:
                return T1;
            default:
                return None;
        }
    }

    /**
     *
     * @param t - several tiers.
     * @return the minimum of the tiers under the precedence NONE &lt; T0 &lt;
     * T1.
     */
    public static Tier min(Tier... t) {
        if (t.length == 0) {
            return Tier.None;
        }
        Tier res = t[0];
        for (int i = 0; i < t.length; i++) {
            if (t[i] == null || t[i] == Tier.None) {
                return Tier.None;
            }
            if (res.toNum() > t[i].toNum()) {
                res = t[i];
            }
        }
        return res;
    }

    /**
     *
     * @param t - several tiers.
     * @return the maximum of the given tiers under the precedence T0 &lt; T1
     * &lt; NONE.
     */
    public static Tier max(Tier... t) {
        if (t.length == 0) {
            return Tier.None;
        }
        Tier res = t[0];
        for (int i = 0; i < t.length; i++) {
            if (t[i] == null || t[i] == Tier.None) {
                return Tier.None;
            }
            if (res.toNum() < t[i].toNum()) {
                res = t[i];
            }
        }
        return res;
    }

    /**
     *
     * @return an integer representation of the current tier.
     */
    public int toNum() {
        switch (this) {
            case T0:
                return 0;
            case T1:
                return 1;
            default:
                return -1;
        }
    }

    /**
     * This method is used to declassify tier types in the statement_declass
     * visitor. This method must be modified if the Tier enum is modified.
     *
     * @return the negation over tiers.
     */
    public Tier next() {
        switch (this) {
            case T0:
                return T1;
            case T1:
                return T0;
            default:
                return None;
        }
    }

    /**
     *
     * @param value - a string.
     * @return a tier built from the string parameter.
     */
    public static Tier fromString(String value) {
        Tier res = Tier.None;
        switch (value) {
            case "0":
                res = Tier.T0;
                break;
            case "1":
                res = Tier.T1;
                break;
        }
        return res;
    }

    @Override
    public String toString() {
        if (this == None) {
            return "None";
        }
        return "" + toNum();
    }
}
