/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.types.tieredtypes;

import complexityparser.types.Tier;

public class ClassTieredType extends TieredType {

    private String mother;

    public ClassTieredType(Tier tier, String mother, String type) {
        super(tier, type);
        this.mother = mother;
    }

    public String getMother() {
        return mother;
    }

    @Override
    public String toString() {
        return "ClassTieredType { "
                + "tier = " + getTier()
                + ", noBlock = " + getNoBlock()
                + ", mother = '" + getMother()
                + "' }";
    }
}
