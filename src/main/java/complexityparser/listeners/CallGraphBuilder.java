/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.listeners;

import complexityparser.analyse.antlr.JavaParser;
import complexityparser.analyse.antlr.JavaParserBaseListener;
import complexityparser.tos.TOS;
import lib.graph.Graph;
import lib.graph.Node;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * A class representing the call graph of a program. The strongly connected
 * components correspond to recursive and mutually recursive methods.
 */
public class CallGraphBuilder extends JavaParserBaseListener {

    private Graph graph;
    private ParseTreeProperty<Integer> blockNumbers;
    private ParseTreeProperty<String> types;
    private TOS tos;
    private String currentNamespace;

    /**
     * The table of symbols (tos) containing the tiered types of each symbol is
     * used to build the method namespaces at the call and declaration
     * locations.
     *
     * @param tosBuilder - an object for building the tos.
     * @param typingListener - an object for builing the method namespace.
     */
    public CallGraphBuilder(TOSBuilder tosBuilder, TypingListener typingListener) {
        this.blockNumbers = tosBuilder.getBlockNumbers();
        this.types = typingListener.getTypes();
        tos = TOS.getInstance();
        currentNamespace = tos.getNameSpace(0);
        graph = new Graph();
    }

    /**
     * @param ctx - a parse tree.
     * @return the block number associated to the parse tree ctx in the
     * {@link CallGraphBuilder#blockNumbers} field.
     */
    private int getBlockNumber(ParseTree ctx) {
        return blockNumbers.get(ctx);
    }

    /**
     * @param ctx - a parse tree
     * @return a string representation of the type of the expression
     * corresponding to the parse tree ctx.
     */
    private String getType(ParseTree ctx) {
        return types.get(ctx);
    }

    /**
     *
     * @return the graph.
     */
    public Graph getGraph() {
        return graph;
    }

    /**
     * Handles method declarations in the parse tree by creating the method
     * namespace and by creating a corresponding node in the call graph. If the
     * node already exists then its context attribute is set to the method
     * declaration context ctx.
     *
     * @param  ctx - a parse tree.
     */
    @Override
    public void enterMethodDeclaration(JavaParser.MethodDeclarationContext ctx) {
        super.enterMethodDeclaration(ctx);
        String namespace = tos.getMethodNamespace(ctx, getBlockNumber(ctx));
        currentNamespace = namespace;
        Node n = graph.get(namespace);
        if (n == null) {
            graph.createNode(namespace, ctx);
        } else {
            n.setCtx(ctx);
        }
    }

    /**
     * see
     * {@link #enterMethodDeclaration(JavaParser.MethodDeclarationContext ctx)}.
     *
     * @param ctx - a constructor declaration context.
     */
    @Override
    public void enterConstructorDeclaration(JavaParser.ConstructorDeclarationContext ctx) {
        super.enterConstructorDeclaration(ctx);
        currentNamespace = "constructor";
    }

    /**
     * Handles the exit of a method declaration block.
     *
     * @param ctx - a method declaration context.
     */
    @Override
    public void exitMethodDeclaration(JavaParser.MethodDeclarationContext ctx) {
        super.exitMethodDeclaration(ctx);
        currentNamespace = null;
    }

    /**
     * Handles the exit of a constructor declaration block.
     *
     * @param ctx - a constructor declaration context.
     */
    @Override
    public void exitGenericConstructorDeclaration(JavaParser.GenericConstructorDeclarationContext ctx) {
        super.exitGenericConstructorDeclaration(ctx);
        currentNamespace = null;
    }

    /**
     * Handles method calls by creating their namespace and by adding an arrow
     * from the current namespace to the namespace of the called function in the
     * graph.
     *
     * @param ctx - an expression call context.
     */
    @Override
    public void exitExpression_call(JavaParser.Expression_callContext ctx) {
        super.enterExpression_call(ctx);
        ArrayList<String> param = new ArrayList<>();
        String name = ctx.methodCall().IDENTIFIER().getText();
        String type;
        /*
        Checks for parameters and populates the param list with a string representation of their java type
        (to create the method namespace later).
         */
        if (ctx.methodCall().expressionList() != null) {
            List<JavaParser.ExpressionContext> list = ctx.methodCall().expressionList().expression();
            for (JavaParser.ExpressionContext e : list) {
                String t = getType(e);
                param.add(t);
            }
        } else if (name.equals("clone")) {
            //Adds no arrow for the clone method
            //since it is expected to be implemented by the language not the programmer.
            return;
        }
        /*
        Checks the receiver expression type, if it is not present the type of the current object is used.
         */
        if (ctx.expression() != null) {
            type = getType(ctx.expression());
        } else {
            int no = getBlockNumber(ctx);
            type = tos.getClassName(no);
        }
        String namespace = TOS.buildMethodNamespace(param, type, name);
        //No check (currentNamespace == null) should be necessary.
        //If currentNamespace == null then a bug is present but elsewhere (probably in this class).
        if (!currentNamespace.equals("constructor")) {
            graph.addConnection(currentNamespace, namespace);
        }
        //If the method calls itself (so not mutually recursive) then a special property needs to be added
        //to simplify the difference between recursive methods and mutually recursive methods.
        if (namespace.equals(currentNamespace)) {
            graph.get(currentNamespace).setRecursive(true);
        } else {
            String cn = currentNamespace;
            while (!cn.contains("<root>::Object::") && !cn.equals(namespace) && !cn.equals("constructor")) {
                cn = tos.getParentNamespace(cn);
            }
            if (cn.equals(namespace)) {
                graph.get(currentNamespace).setRecursive(true);
            }
            cn = namespace;
            while (!cn.contains("<root>::Object::") && !cn.equals(currentNamespace) && !cn.equals("constructor")) {
                cn = tos.getParentNamespace(cn);
            }
            if (cn.equals(currentNamespace)) {
                graph.get(currentNamespace).setRecursive(true);
            }
        }
    }
}