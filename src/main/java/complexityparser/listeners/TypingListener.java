/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.listeners;

import complexityparser.analyse.antlr.JavaParser;
import complexityparser.analyse.antlr.JavaParserBaseListener;
import complexityparser.tos.TOS;
import complexityparser.tos.id.VariableID;
import complexityparser.types.tieredtypes.MethodTieredType;
import complexityparser.types.tieredtypes.TieredType;
import complexityparser.types.tieredtypes.VariableTieredType;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * The TypingListener associates types to expression nodes of the parse tree.
 * The class and its methods are used to create the namespace of called methods
 * from their caller. Namespaces are used to identify uniquely a method.
 */
public class TypingListener extends JavaParserBaseListener {

    private ParseTreeProperty<String> types;
    private ParseTreeProperty<Integer> blockNumbers;
    private TOS tos;

    public TypingListener(ParseTreeProperty<Integer> blockNumbers) {
        this.blockNumbers = blockNumbers;
        types = new ParseTreeProperty<>();
        tos = TOS.getInstance();
    }

    public ParseTreeProperty<String> getTypes() {
        return types;
    }

    @Override
    public void exitPrimary_literal(JavaParser.Primary_literalContext ctx) {
        super.exitPrimary_literal(ctx);
        types.put(ctx, types.get(ctx.literal()));
    }

    @Override
    public void exitPrimary_expression(JavaParser.Primary_expressionContext ctx) {
        super.exitPrimary_expression(ctx);
        types.put(ctx, types.get(ctx.expression()));
    }

    @Override
    public void exitPrimary_identifier(JavaParser.Primary_identifierContext ctx) {
        super.exitPrimary_identifier(ctx);
        VariableID id = new VariableID(ctx.IDENTIFIER().getText());
        TieredType t = tos.find(id, blockNumbers.get(ctx));
        if (t != null) {
            types.put(ctx, t.getType());
        } else {
            types.put(ctx, "unknown");
        }
    }

    @Override
    public void exitPrimary_this(JavaParser.Primary_thisContext ctx) {
        super.exitPrimary_this(ctx);
        int no = blockNumbers.get(ctx);
        String type = tos.getClassName(no);
        types.put(ctx, type);
    }

    /**
     * @param op - a String encoding an operator symbol.
     * @return true if the string encodes an operator that returns a boolean
     * value.
     */
    private boolean isBooleanOperator(String op) {
        String[] booleanOperators = {"<", ">", ">=", "<=", "||", "&&"};
        boolean res = false;
        for (int i = 0; i < booleanOperators.length && !res; i++) {
            res = booleanOperators[i].equals(op);
        }
        return res;
    }

    @Override
    public void exitExpression_operation(JavaParser.Expression_operationContext ctx) {
        super.exitExpression_operation(ctx);
        if (isBooleanOperator(ctx.bop.getText())) {
            types.put(ctx, "boolean");
        } else {
            types.put(ctx, types.get(ctx.expression(0)));
        }
    }

    @Override
    public void exitExpression_call(JavaParser.Expression_callContext ctx) {
        super.exitExpression_call(ctx);
        /*
        In order to know the return type of a method call, the method needs to be uniquely determined.
        The namespace of a method is built to achieve this.
         */
        ArrayList<String> param = new ArrayList<>();
        String name = ctx.methodCall().IDENTIFIER().getText();
        String type;
        if (ctx.expression() != null) {
            type = types.get(ctx.expression());
        } else {
            int no = blockNumbers.get(ctx);
            type = tos.getClassName(no);
        }
        if (name.equals("clone")) {
            types.put(ctx, type);
        } else {
            if (ctx.methodCall().expressionList() != null) {
                List<JavaParser.ExpressionContext> list = ctx.methodCall().expressionList().expression();
                for (JavaParser.ExpressionContext e : list) {
                    String t = types.get(e);
                    param.add(t);
                }
            }
            /*
           The method namespace is built in the TOS.
             */
            String namespace = TOS.buildMethodNamespace(param, type, name);
            MethodTieredType mt = tos.findMethodTieredTypeByNamespace(namespace);
            if (mt != null) {
                /*
                If the method tiered type has been found, then its Java return type is
                added in the attribute types.
                 */
                types.put(ctx, mt.getType());
            }
        }
    }

    @Override
    public void exitExpression_identifier(JavaParser.Expression_identifierContext ctx) {
        super.exitExpression_identifier(ctx);
        int no = blockNumbers.get(ctx);
        no = tos.getClassBlockNumber(no);
        String name = ctx.IDENTIFIER().getText();
        VariableID id = new VariableID(name);
        VariableTieredType t = (VariableTieredType) tos.find(id, no);
        if (t != null) {
            types.put(ctx, t.getType());
        }
    }

    @Override
    public void exitExpression_new(JavaParser.Expression_newContext ctx) {
        super.exitExpression_new(ctx);
        types.put(ctx, ctx.creator().createdName().getText());
    }

    @Override
    public void exitLiteral_integer(JavaParser.Literal_integerContext ctx) {
        super.exitLiteral_integer(ctx);
        types.put(ctx, "int");
    }

    @Override
    public void exitLiteral_boolean(JavaParser.Literal_booleanContext ctx) {
        super.exitLiteral_boolean(ctx);
        types.put(ctx, "boolean");
    }

    @Override
    public void exitExpression_primary(JavaParser.Expression_primaryContext ctx) {
        super.exitExpression_primary(ctx);
        types.put(ctx, types.get(ctx.primary()));
    }

    @Override
    public void exitExpression_unary(JavaParser.Expression_unaryContext ctx) {
        super.exitExpression_unary(ctx);
        types.put(ctx, types.get(ctx.expression()));
    }
}
