/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.tos.blocks;

import java.util.ArrayList;

/**
 * A block representation for methods.
 */
public class MethodBlock extends Block {

    private ArrayList<String> parameters;

    public MethodBlock(String name, ArrayList<String> p) {
        super(name);
        parameters = p;
    }

    @Override
    public String getBlockName() {
        StringBuilder param = new StringBuilder(parameters.toString());
        param.setCharAt(0, '(');
        param.setCharAt(param.length() - 1, ')');
        return getName() + param;
    }
}
