/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.tos;

import complexityparser.analyse.antlr.JavaParser;
import complexityparser.tos.blocks.Block;
import complexityparser.tos.blocks.ClassBlock;
import complexityparser.tos.blocks.MethodBlock;
import complexityparser.tos.id.ClassID;
import complexityparser.tos.id.ID;
import complexityparser.tos.id.MethodID;
import complexityparser.types.tieredtypes.ClassTieredType;
import complexityparser.types.tieredtypes.MethodTieredType;
import complexityparser.types.tieredtypes.TieredType;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * Table Of Symbols.
 *
 * Lists all the program variables and their tiered types for each block.
 */
public class TOS {

    private static TOS tos = null;
    private ArrayList<Block> array;
    private int currentBlock;
    private PrintStream err;

    /**
     *
     * @return the singleton instance tos.
     */
    public static TOS getInstance() {
        if (tos == null) {
            tos = new TOS();
        }
        return tos;
    }

    private TOS() {
        array = new ArrayList<>();
        clear();
    }

    /**
     * Clears the tos and initialises it with the <root> block (0).
     */
    public void clear() {
        array.clear();
        currentBlock = 0;
        array.add(new Block("<root>", 0, -1));
        err = new PrintStream(new ByteArrayOutputStream());
    }

    /**
     * Enters a new block in the tos.
     *
     * @param name - the name of the block.
     */
    public void enterBlock(String name) {
        array.add(new Block(name));
        currentBlock = array.size() - 1;
    }

    /**
     * Sets the current block to a new class block.
     *
     * @param name - the name of the block.
     * @param parent - the parent of the block.
     */
    public void enterClass(String name, String parent) {
        if (currentBlock != 0) {
            err.println("Cannot create class in another block than <root>");
            return;
        }
        array.add(new ClassBlock(name, parent));
        currentBlock = array.size() - 1;
    }

    /**
     * Sets the current block to a new method (body) block
     *
     * @param name - the name of the method block.
     * @param param - the list of parameter names.
     */
    public void enterMethod(String name, ArrayList<String> param) {
        array.add(new MethodBlock(name, param));
        currentBlock = array.size() - 1;
    }

    /**
     * Updates the current block to the parent block.
     */
    public void exitBlock() {
        currentBlock = array.get(currentBlock).getNoParent();
    }

    /**
     *
     * @return the index of the current block.
     */
    public int getCurrentBlock() {
        return currentBlock;
    }

    /**
     *
     * @param n - a block index.
     * @return the index of the parent block.
     */
    public int getParentBlock(int n) {
        return array.get(n).getNoParent();
    }

    /**
     * Adds a new entry into the current block. Every ID has to be unique inside
     * the current block. No check is done before or after inserting the ID so
     * if an ID is redefined it will be overwritten without any warning.
     *
     * @param i - the identifier of the entry.
     * @param t - the tiered type of the entry.
     */
    public void put(ID i, TieredType t) {
        array.get(currentBlock).put(i, t);
    }

    /**
     * Adds a class id and a tiered type in the current block. If it already
     * exists the previous tiered type is overriden.
     *
     * @param i - a class ID.
     * @param t - a class tiered type.
     */
    public void put(ClassID i, ClassTieredType t) {
        if (currentBlock != 0) {
            err.println("Cannot create class in another block than <root>");
            return;
        }
        array.get(currentBlock).put(i, t);
    }

    /**
     *
     * @return the index associated to the next block (total number of blocks).
     */
    public int getNextBlockNumber() {
        return array.size();
    }

    /**
     *
     * @param no - an integer.
     * @return the namespace for the given block number.
     */
    public String getNameSpace(int no) {
        StringBuilder str = new StringBuilder();
        while (no != -1) {
            str.insert(0, array.get(no).getBlockName());
            if (no != 0) {
                str.insert(0, "::");
            }
            no = array.get(no).getNoParent();
        }
        return str.toString();
    }

    /**
     *
     * @param i - an ID.
     * @param noBloc - a number representing a block index.
     * @return a tiered type associated with the specified id inside the block of
     * index noBloc (or one of its parent blocks).
     */
    public TieredType find(ID i, int noBloc) {
        TieredType res = null;
        while (res == null && noBloc != -1) {
            Block b = array.get(noBloc);
            res = b.get(i);
            if (res == null) {
                /*
                If the current block is a class and the tiered type has not been found
                (since classes cannot be declared inside other classes) then the parent block is necessarily
                <root>  (and will not contain the searched value).
		Moreover it also means that it is time to start looking
                in the parent classes of the current class because fields are inherited.
                 */
                if (b.getNoParent() != -1 && b instanceof ClassBlock) {
                    String parent = ((ClassBlock) b).getMother();
                    int j;
                    for (j = 0; j < array.size(); j++) {
                        if (array.get(j).getBlockName().equals(parent)) {
                            noBloc = j;
                            break;
                        }
                    }
                    if (j == array.size()) {
                        noBloc = b.getNoParent();
                    }
                } else {
                    noBloc = b.getNoParent();
                }
            }
        }
        return res;
    }

    /**
     *
     * @param str - a namespace.
     * @return the name of the parent class for the given namespace.
     */
    public String getMotherByName(String str) {
        for (Block b : array) {
            if (b.getName().equals(str)) {
                return b.getMother();
            }
        }
        return "Object";
    }

    /**
     *
     * @param namespace - a namespace.
     * @return the namespace associated to the superclass of the class of the
     * parameter namespace. If it corresponds to a method the namespace of the
     * overridden method is returned.
     */
    public String getParentNamespace(String namespace) {
        String[] names = namespace.split("::");
        if (names.length != 3) {
            return "";
        }
        names[1] = getMotherByName(names[1]);
        return names[0] + "::" + names[1] + "::" + names[2];
    }

    /**
     *
     * @param namespace - a namespace.
     * @return the namespace of a method that is at the root of the class
     * hierarchy.
     */
    public String getRootNamespace(String namespace) {
        String res = namespace;
        while (!namespace.contains("<root>::Object::") && namespace.split("::").length == 3) {
            res = namespace;
            namespace = getParentNamespace(namespace);
        }
        return res;
    }

    /**
     *
     * @param namespace - a namespace.
     * @return the namespaces of all the hypothetically overridden methods
     * contained in the children of the class (with the method name contained in
     * the given namespace).
     */
    public String[] getChildrenNamespaces(String namespace) {
        String[] names = namespace.split("::");
        ArrayList<String> res = new ArrayList<>();
        for (Block b : array) {
            if (b.getMother().equals(names[1])) {
                String tmp = "<root>::" + b.getName() + "::" + names[2];
                res.add(tmp);
            }
        }
        return res.toArray(new String[0]);
    }

    /**
     *
     * @param namespace - a namespace.
     * @return the method tiered type identified by the given namespace.
     */
    public MethodTieredType findMethodTieredTypeByNamespace(String namespace) {
        MethodTieredType res = null;
        String[] names = namespace.split("::");
        if (names.length != 3) {
            return null;
        }
        for (Block b : array) {
            if (b.getBlockName().equals(names[1])) {
                MethodTieredType t = b.findMethodTieredTypeByNamespace(names[2]);
                if (t != null) {
                    return t;
                } else {
                    namespace = "<root>::" + b.getMother() + "::" + names[2];
                    return findMethodTieredTypeByNamespace(namespace);
                }
            }
        }
        return res;
    }

    /**
     *
     * @return a string representation of the tos.
     */
    public String toString() {
        StringBuilder str = new StringBuilder();
        Stack<Pair> stack = new Stack<>();
        stack.push(new Pair(array.get(0), 0));
        String prefix = "";
        while (!stack.empty()) {
            Pair p = stack.peek();
            if (p.b.getNo() == p.index) {
                str.append(prefix + p.b.getBlockName() + " {\n");
                prefix = "   " + prefix;
                for (Map.Entry<ID, TieredType> entry : p.b.getMap()) {
                    str.append(prefix);
                    str.append(entry.getKey() + " -> " + entry.getValue() + "\n");
                }
                p.index++;
            } else if (p.index >= array.size()) {
                prefix = prefix.substring(0, prefix.length() - 3);
                str.append(prefix + "}\n");
                stack.pop();
            } else {
                while (p.index < array.size() && p.b.getNo() != array.get(p.index).getNoParent()) {
                    p.index++;
                }
                if (p.index >= array.size()) {
                    prefix = prefix.substring(0, prefix.length() - 3);
                    str.append(prefix + "}\n");
                    stack.pop();
                } else {
                    Block tmp = array.get(p.index);
                    stack.push(new Pair(tmp, tmp.getNo()));
                }
                p.index++;
            }
        }
        return str.toString();
    }

    public String getError() {
        return err.toString();
    }

    /**
     * A simple inner class used in the toString method.
     */
    private class Pair {

        public Block b;
        public int index;

        public Pair(Block b, int index) {
            this.b = b;
            this.index = index;
        }
    }

    /**
     *
     * @param params - a list of parameter names.
     * @param names - several class and method names.
     * @return the namespace that can be created with the given method
     * parameters and the class names and method names given as parameter.
     */
    public static String buildMethodNamespace(ArrayList<String> params, String... names) {
        String[] tmp = new String[names.length + 1];
        tmp[0] = "<root>";
        System.arraycopy(names, 0, tmp, 1, names.length);
        return appendToNamespace(params, tmp);
    }

    /**
     *
     * @param ctx - a declaration context.
     * @param block - the index of a block.
     * @return builds the namespace of a method using its parse tree and a block
     * number (for the given node).
     */
    public String getMethodNamespace(JavaParser.MethodDeclarationContext ctx, int block) {
        ArrayList<String> params = new ArrayList<>();
        String name = ctx.IDENTIFIER().getText();
        if (ctx.formalParameters().formalParameterList() != null) {
            List<JavaParser.FormalParameterContext> list = ctx.formalParameters().formalParameterList().formalParameter();
            for (JavaParser.FormalParameterContext p : list) {
                params.add(p.typeType().getText());
            }
        }
        MethodID id = new MethodID(name, params);
        MethodTieredType t = (MethodTieredType) tos.find(id, block);
        t.setTier(null);
        String namespace = tos.getNameSpace(t.getMethodBlock());
        return namespace;
    }

    public String getConstructorNamespace(JavaParser.ConstructorDeclarationContext ctx, int block) {
        ArrayList<String> params = new ArrayList<>();
        String name = ctx.IDENTIFIER().getText();
        if (ctx.formalParameters().formalParameterList() != null) {
            List<JavaParser.FormalParameterContext> list = ctx.formalParameters().formalParameterList().formalParameter();
            for (JavaParser.FormalParameterContext p : list) {
                params.add(p.typeType().getText());
            }
        }
        MethodID id = new MethodID(name, params);
        MethodTieredType t = (MethodTieredType) tos.find(id, block);
        t.setTier(null);
        String namespace = tos.getNameSpace(t.getMethodBlock());
        return namespace;
    }

    /**
     *
     * @param no - an index.
     * @return the parent class block number of the block number of the specified index.
     */
    public int getClassBlockNumber(int no) {
        while (no != -1 && !(array.get(no) instanceof ClassBlock)) {
            no = array.get(no).getNoParent();
        }
        return no;
    }

    /**
     *
     * @param no - an index.
     * @return the number of the parent class for the block number corresponding to the specified index.
     */
    public int getMotherClassNumber(int no) {
        if (no == -1) {
            return no;
        }
        no = getClassBlockNumber(no);

        ClassBlock b = (ClassBlock) array.get(no);
        String mother = b.getMother();
        for (int i = 0; i < array.size(); i++) {
            if (array.get(i).getName().equals(mother)) {
                return i;
            }
        }
        return -1;
    }

    /**
     *
     * @param no - an index.
     * @param cl - an index.
     * @return true if the block of number of index no is contained inside the class
     * block of index cl.
     */
    public boolean isBlockInClass(int no, int cl) {
        if (no == -1) {
            return false;
        }
        no = getClassBlockNumber(no);
        if (no == cl) {
            return true;
        }
        return isBlockInClass(getMotherClassNumber(no), cl);
    }

    /**
     *
     * @param no - an index.
     * @return the name of the class identified by the block number of index no.
     */
    public String getClassName(int no) {
        String res = "";
        no = getClassBlockNumber(no);
        if (no != -1) {
            res = array.get(no).getBlockName();
        }
        return res;
    }

    /**
     *
     * @param params - a list of parameter names.
     * @param names - several names.
     * @return a string containing the namespace given by the names array
     * appended with the given method parameters
     */
    public static String appendToNamespace(ArrayList<String> params, String... names) {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < names.length; i++) {
            res.append(names[i]);
            if (i < names.length - 1) {
                res.append("::");
            }
        }
        res.append("(");
        for (int i = 0; i < params.size(); i++) {
            res.append(params.get(i));
            if (i < params.size() - 1) {
                res.append(", ");
            }
        }
        res.append(")");
        return res.toString();
    }
}
