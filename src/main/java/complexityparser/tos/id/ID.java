/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.tos.id;

import java.util.Objects;

/**
 * An abstract class for representing an ID.
 */
public abstract class ID {

    private String name;

    public ID(String name) {
        this.name = name;
    }

    /**
     *
     * @return the name of the ID
     */
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "ID { "
                + "name = '" + name + "\' "
                + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ID id = (ID) o;
        return Objects.equals(name, id.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
