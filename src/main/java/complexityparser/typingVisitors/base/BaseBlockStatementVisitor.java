/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.typingVisitors.base;

import complexityparser.analyse.antlr.JavaParser;
import complexityparser.listeners.TOSBuilder;
import complexityparser.listeners.TypingListener;
import complexityparser.tos.id.VariableID;
import complexityparser.types.tieredtypes.VariableTieredType;
import complexityparser.types.Tier;
import org.antlr.v4.runtime.CommonTokenStream;

/**
 * Abstract class for the base visitor extending BaseStatementVisitor to handle
 * BlockStatements.
 */
public abstract class BaseBlockStatementVisitor extends BaseStatementVisitor {

    public BaseBlockStatementVisitor(TOSBuilder tosBuilder, TypingListener typingListener, CommonTokenStream stream) {
        super(tosBuilder, typingListener, stream);
    }

    /**
     * Handles variable declarations.
     *
     * @param ctx - a local variable declaration context.
     * @return the tier of the block.
     */
    @Override
    public Object visitLocalVariableDeclaration(JavaParser.LocalVariableDeclarationContext ctx) {
        visitChildren(ctx);
        Tier res = Tier.T0;
        if (isInInitBlock()) {
            res = Tier.T1;
        }
        if (ctx.TIER() != null) {
            String tmp = ctx.TIER().getText();
            tmp = tmp.substring(1, tmp.length() - 1);
            res = Tier.toTier(Integer.valueOf(tmp));
        }
        Tier min = res;
        Tier expected = res;
        Tier env = Tier.T0;
        for (JavaParser.VariableDeclaratorContext var : ctx.variableDeclarators().variableDeclarator()) {
            String name = var.variableDeclaratorId().getText();
            VariableID id = new VariableID(name);
            int block = getBlockNumber(ctx);
            VariableTieredType t = (VariableTieredType) tos.find(id, block);
            if (t != null) {
                //Sets the tier of each variable since a declass block might have modified the original tier value in a previous pass.
                t.setTier(expected);
            }
            if (var.variableInitializer() != null) {
                //A variable declaration may contain multiple variables, thus every variable needs to be checked.
                if (ctx.typeType().getText().equals("int") || ctx.typeType().getText().equals("boolean")) {
                    if (Tier.min(res, getTier(var.variableInitializer().expression())) != res) {
                        res = Tier.None;
                    }
                } else {
                    if (res != getTier(var.variableInitializer().expression())) {
                        res = Tier.None;
                    }
                }
                env = Tier.max(env, getEnvironmentTier(var.variableInitializer().expression()), getTier(var.variableInitializer().expression()));
            }
        }
        res = Tier.max(res, env);
        putTier(ctx, res, Tier.T0);
        return res;
    }

    /**
     * Copies the tier of a variable declaration context to its associated block
     * statement parse tree.
     *
     * @param ctx - a local variable declaration context.
     * @return the copied tier.
     */
    @Override
    public Object visitBlockStatement_localVariableDeclaration(JavaParser.BlockStatement_localVariableDeclarationContext ctx) {
        visitChildren(ctx);
        return copyTier(ctx.localVariableDeclaration(), ctx);
    }

    /**
     * Copies the tier type of a block statement context to its associated block
     * statement parse tree.
     *
     * @param ctx - a block statement context.
     * @return the copied tier.
     */
    @Override
    public Object visitBlockStatement_statement(JavaParser.BlockStatement_statementContext ctx) {
        visitChildren(ctx);
        return copyTier(ctx.statement(), ctx);
    }
}
