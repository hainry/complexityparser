/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package complexityparser.typingVisitors.base;

import complexityparser.types.Tier;
import complexityparser.types.env.TypingEnv;
import complexityparser.analyse.antlr.JavaParser;
import complexityparser.analyse.antlr.JavaParserBaseVisitor;
import complexityparser.listeners.TOSBuilder;
import complexityparser.listeners.TypingListener;
import complexityparser.tos.TOS;
import lib.Logger;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import java.util.HashSet;

/**
 * Base class used by the tier analysis.
 */
public abstract class BaseVisitor extends JavaParserBaseVisitor {

    private ParseTreeProperty<Tier> tiers; //The tier for each node of the parse tree.
    private ParseTreeProperty<Tier> environment; //The block (environment) tier for each node of the parse tree. It is used for method calls in expressions.
    private Tier actualEnvironment; //The current block (environment) tier. Used for fields.
    private ParseTreeProperty<String> types; //The types for each node of the parse tree.
    private Tier result; //The tier of the final result.
    private String currentNamespace; //The current namespace.
    private HashSet<String> recursiveNamespaces; //The namespaces for mutually recursive methods.
    private int recursiveCallsCount; //The number of recursive calls encountered in the current block.
    private HashSet<String> recursiveCallsReceivers; //The hashset of recursive calls receivers names.
    private int whileCount; //The number of while statements encountered in the current block.
    private ParseTreeProperty<Integer> blockNumbers; //The block index associated to the each node of the parse tree.
    protected TOS tos; //The table of symbols.
    protected TypingEnv te; //The typing environment (HashMap<String, FOTierList>).
    private CommonTokenStream commonTokenStream; //ANTLR token stream to show line numbers.
    private boolean overrideResult = true; //A flag to set if the final result should be overridden and if a string output should be generated.
    private boolean inInitBlock; //A flag to indicate if the visitor is currently located inside an init block or not.
    private Tier literalDefaultTier; //The default tier used to type literals. This value changes if a block is forced to type in 0.

    public BaseVisitor(TOSBuilder tosBuilder, TypingListener typingListener, CommonTokenStream stream) {
        this.blockNumbers = tosBuilder.getBlockNumbers();
        tiers = new ParseTreeProperty<>();
        environment = new ParseTreeProperty<>();
        actualEnvironment = Tier.None;
        this.types = typingListener.getTypes();
        commonTokenStream = stream;
        tos = TOS.getInstance();
        te = TypingEnv.getInstance();
        currentNamespace = tos.getNameSpace(0);
        recursiveCallsCount = 0;
        recursiveCallsReceivers = new HashSet<>();
        whileCount = 0;
        recursiveNamespaces = new HashSet<>();
        inInitBlock = false;
        literalDefaultTier = Tier.T1;
    }

    /**
     *
     * @return true if the visitor is located inside an init block, false
     * otherwise.
     */
    public boolean isInInitBlock() {
        return inInitBlock;
    }

    public HashSet<String> getRecursiveNamespaces() {
        return recursiveNamespaces;
    }

    /**
     * Sets a flag to indicate if the visitor is currently inside an init block
     * or not.
     *
     * @param inInitBlock - a boolean value.
     */
    public void setInInitBlock(boolean inInitBlock) {
        this.inInitBlock = inInitBlock;
    }

    /**
     *
     * @return the current tier to use to type literals.
     */
    public Tier getLiteralDefaultTier() {
        return literalDefaultTier;
    }

    /**
     * Sets the default tier used to type literals.
     *
     * @param literalDefaultTier - a new default tier.
     */
    public void setLiteralDefaultTier(Tier literalDefaultTier) {
        this.literalDefaultTier = literalDefaultTier;
    }

    /**
     * Resets the tier used to type literals.
     */
    public void resetPrimitiveDefaultTier() {
        literalDefaultTier = Tier.T1;
    }

    /**
     * Adds a namespace to the list (the list of mutually recursive methods
     * contain strictly more than 1 method namespace).
     *
     * @param name - a method namespace.
     */
    public void addRecursiveNamespace(String name) {
        recursiveNamespaces.add(name);
    }

    /**
     * Clears the array containing the namespaces for the mutually recursive
     * methods.
     */
    public void clearRecursiveNamespaces() {
        recursiveNamespaces.clear();
    }

    /**
     *
     * @param name - a method namespace.
     * @return true if the given namespace is contained in the current list of
     * mutually recursive methods, false otherwise.
     */
    public boolean containsRecursiveNamespace(String name) {
        return recursiveNamespaces.contains(name);
    }

    /**
     * Increments the count of recursive calls.
     */
    public void incrementRecursiveCallsCount(String name) {
        if (recursiveCallsReceivers.size() == 0) {
            recursiveCallsCount++;
        }
        if (recursiveCallsReceivers.contains(name)) {
            recursiveCallsCount++;
        } else {
            recursiveCallsReceivers.add(name);
        }
    }

    /**
     *
     * @return a hashset containing all the receivers used until now for
     * recursive calls.
     */
    public HashSet<String> getRecursiveCallsReceivers() {
        return recursiveCallsReceivers;
    }

    /**
     * Sets the hashset containing all the receivers used until now for
     * recursive calls to the parameter.
     *
     * @param recursiveCallsReceivers - a hashset of namespaces.
     */
    public void setRecursiveCallsReceivers(HashSet<String> recursiveCallsReceivers) {
        this.recursiveCallsReceivers = recursiveCallsReceivers;
    }

    /**
     * Increments the while count.
     */
    public void incrementWhileCount() {
        whileCount++;
    }

    /**
     *
     * @return the number of while loops encountered.
     */
    public int getWhileCount() {
        return whileCount;
    }

    /**
     * Sets the while count to the given value.
     *
     * @param whileCount - a number of while encountered.
     */
    public void setWhileCount(int whileCount) {
        this.whileCount = whileCount;
    }

    /**
     *
     * @return the number of recursive calls encountered.
     */
    public int getRecursiveCallsCount() {
        return recursiveCallsCount;
    }

    /**
     * Sets the number of recursive calls to the given value.
     *
     * @param val - a number of recursive calls encountered.
     */
    public void setRecursiveCallsCount(int val) {
        recursiveCallsCount = val;
    }

    /**
     *
     * @param ctx - a parse tree.
     * @return the block index associated with the given tree node.
     */
    public Integer getBlockNumber(ParseTree ctx) {
        return blockNumbers.get(ctx);
    }

    /**
     * Sets the value of override result: if true, the result value will be
     * overriden on call of putTier(..) and a string output will be
     * generated else nothing of the above will be done.
     *
     * @param overrideResult
     */
    protected void setOverrideResult(boolean overrideResult) {
        this.overrideResult = overrideResult;
    }

    /**
     *
     * @return the result of the analysis.
     */
    public Tier getResult() {
        return result;
    }

    /**
     * Updates the final result to the given value.
     *
     * @param result - a tier.
     */
    protected void setResult(Tier result) {
        this.result = result;
    }

    /**
     * Associates a tier and a block tier to a parse tree. If the visitor is
     * located inside an init block nothing is done.
     *
     * @param ctx - a parse tree.
     * @param t - a tier.
     * @param env - a block (environment) tier.
     */
    protected void putTier(ParseTree ctx, Tier t, Tier env) {
        if (!isInInitBlock()) {
            tiers.put(ctx, t);
            environment.put(ctx, env);
            if (overrideResult) {
                if (result != Tier.None) {
                    result = t;
                }
                String name = ctx.getClass().toString();
                name = name.replace("class complexityparser.analyse.antlr.JavaParser$", "");
                name = name.replace("Context", "");
                Interval src = ctx.getSourceInterval();
                Token first = commonTokenStream.get(src.a);
                int line = first.getLine();
                print(name, ctx.getText(), t, env, line);
            }
        }
    }

    /**
     * Updates the values tier and type associated to the parse tree dst to the
     * values of tier and type of src in the tiers attribute.
     *
     * @param src - a parse tree.
     * @param dst - a parse tree.
     * @return the value of the updated tier.
     */
    protected Tier copyTier(ParseTree src, ParseTree dst) {
        Tier r = getTier(src);
        putTier(dst, r, getEnvironmentTier(src));
        return r;
    }

    /**
     *
     * @param ctx - a parse tree.
     * @return the environment tier associated with the given parse tree.
     */
    protected Tier getEnvironmentTier(ParseTree ctx) {
        return environment.get(ctx);
    }

    /**
     *
     * @return the current environment tier.
     */
    public Tier getActualEnvironment() {
        return actualEnvironment;
    }

    /**
     * Sets the current environment tier to the given value.
     *
     * @param actualEnvironment - the new current environment tier.
     */
    public void setActualEnvironment(Tier actualEnvironment) {
        this.actualEnvironment = actualEnvironment;
    }

    /**
     *
     * @param ctx - a parse tree.
     * @return the tier associated with the parse tree ctx, may be null.
     */
    protected Tier getTier(ParseTree ctx) {
        return tiers.get(ctx);
    }

    /**
     *
     * @param ctx - a parse tree.
     * @return a string representation of the type associated to the parse tree
     * ctx.
     */
    protected String getType(ParseTree ctx) {
        return types.get(ctx);
    }

    /**
     * A helper function to avoid duplicate code and to print the appropriate
     * message to the appropirate output (error or standard).
     *
     * @param rule - the name of the grammar rule.
     * @param txt - the code associated with the grammar rule.
     * @param t - the tier type of the rule.
     * @param line - the line number of the code (its first token).
     */
    private void print(String rule, String txt, Tier t, Tier env, int line) {
        String message = rule + " {\n\tline:" + line + " ; " + txt + "\n\t-> " + t + "\n\t env: " + env + "\n}";
        if (t == Tier.None || env == Tier.None) {
            Logger.errPrintln("cannot type " + message);
        } else {
            Logger.println(message);
        }
    }

    /**
     * The type analysis for an #init #init block. The tier T0 is used.
     *
     * @param ctx - an init context.
     * @return the tier T0.
     */
    @Override
    public Object visitStatement_init(JavaParser.Statement_initContext ctx) {
        setInInitBlock(true);
        visitChildren(ctx);
        setInInitBlock(false);
        putTier(ctx, Tier.T0, Tier.T0);
        return Tier.T0;
    }
}
