/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package view;

import complexityparser.Model;
import lib.Observable;
import lib.Observer;
import org.antlr.v4.gui.TreeViewer;
import javax.swing.*;
import java.awt.*;

/**
 * A basic pannel displaying the ANTLR tree.
 */
public class TreePanel extends JScrollPane implements Observer {

    private TreeViewer viewr;

    public TreePanel(Model m) {
        super(m.getTreeComponent());
        m.addObserver(this);
        setPreferredSize(new Dimension(600, 600));
        getVerticalScrollBar().setUnitIncrement(10);
        getHorizontalScrollBar().setUnitIncrement(10);
        JScrollBar vertical = getVerticalScrollBar();
        InputMap im = vertical.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        im.put(KeyStroke.getKeyStroke("DOWN"), "positiveUnitIncrement");
        im.put(KeyStroke.getKeyStroke("UP"), "negativeUnitIncrement");
        JScrollBar horizontal = getHorizontalScrollBar();
        im = horizontal.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        im.put(KeyStroke.getKeyStroke("RIGHT"), "positiveUnitIncrement");
        im.put(KeyStroke.getKeyStroke("LEFT"), "negativeUnitIncrement");
    }

    @Override
    public void update(Observable m) {
        Model model = (Model) m;
        if (model.getSyntaxError()) {
            viewr = new TreeViewer(null, null);
        } else {
            viewr = model.getTreeComponent();
        }
        setViewportView(viewr);
    }
}
