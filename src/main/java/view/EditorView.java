/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package view;

import complexityparser.Model;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import lib.Observable;
import lib.Observer;

/**
 * A basic view to display a text editor containing the code that is analysed
 * and displayed by the ANTLR tree.
 */
public class EditorView extends JPanel implements Observer {

    private JScrollPane panel;
    private JTextArea area;
    private JButton updateButton;
    private MainFrame parent;
    private Model m;

    public EditorView(Model m, MainFrame parent) {
        m.addObserver(this);
        this.parent = parent;
        this.m = m;
        InputMap inputMap = getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        area = new JTextArea(m.getCode());
        area.setCaretColor(area.getForeground());
        area.getCaret().setBlinkRate(0);
        area.setTabSize(4);
        area.setFont(new Font("monospaced", Font.PLAIN, 12));
        panel = new JScrollPane(area);
        panel.setWheelScrollingEnabled(true);
        updateButton = new JButton("update");
        Action action = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    m.setSyntaxError(false);
                    String code = area.getText();
                    m.setCode(code);
                } catch (IndexOutOfBoundsException e) {
                    area.setText(area.getText());
                    m.setSyntaxError(true);
                    m.update();
                }
            }
        };
        updateButton.addActionListener(action);
        inputMap.put(KeyStroke.getKeyStroke("F6"), "F6");
        getActionMap().put("F6", action);
        BorderLayout layout = new BorderLayout();
        setLayout(layout);
        add(panel, BorderLayout.CENTER);
        add(updateButton, BorderLayout.SOUTH);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK), "o");
        getActionMap().put("o", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser fc = new JFileChooser();
                String s = System.getProperty("user.dir") + System.getProperty("file.separator") + "examples";
                fc.setCurrentDirectory(new File(s));
                if (JFileChooser.APPROVE_OPTION == fc.showOpenDialog(parent)) {
                    File f = fc.getSelectedFile();
                    try {
                        m.setSyntaxError(false);
                        byte[] res = Files.readAllBytes(f.toPath());
                        String txt = new String(res, StandardCharsets.UTF_8);
                        area.setText(txt);
                        m.setCode(txt);
                        parent.setTitle(f.toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (IndexOutOfBoundsException e) {
                        area.setText(area.getText());
                        m.setSyntaxError(true);
                        m.update();
                        parent.setTitle(f.toString());
                    }
                }
            }
        });
        setPreferredSize(new Dimension(400, 600));
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "escape");
        getActionMap().put("escape", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                area.transferFocus();
            }
        });
        update(m);
    }

    @Override
    public void update(Observable m) {
        Model model = (Model) m;
        area.setText(model.getCode());
        panel.setViewportView(area);
    }
}
