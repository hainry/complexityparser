/*
Copyright 2019 ZEYEN Olivier, PÉCHOUX Romain, HAINRY Emmanuel, JEANDEL Emmanuel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package view;

import complexityparser.Model;
import lib.Observable;
import lib.Observer;
import javax.swing.*;
import java.awt.*;

/**
 * This class is used to show error and standard output caught by the model
 * during the type analysis performed by the TierTypingListener class.
 */
public class MessageView extends JScrollPane implements Observer {

    private JTextArea area;
    private Model m;

    //   public void setTextArea(String txt){
    //	this.area = new TextArea(txt);
    //   }	
    public MessageView(Model m) {
        m.addObserver(this);
        this.m = m;
        area = new JTextArea();
        area.setTabSize(3);
        area.setFont(new Font("monospaced", Font.PLAIN, 12));
        area.setEditable(false);
        setViewportView(area);
        setPreferredSize(new Dimension(400, 600));
        update(m);
    }

    @Override
    public void update(Observable o) {
        StringBuilder str = new StringBuilder();
        String out = m.getOutput();
        String err = m.getError();
        if (m.getSyntaxError()) {
            str.append("\n SYNTAX ERROR : This is not a syntactically correct Java \n program.\n");
        } else {
            str.append("Output:\n\n");
            str.append(out);
            if (!err.equals("")) {
                str.append("\n-------------------------------------------------\nErrors:\n\n");
                str.append(m.getError());
            }
            str.append("\n-------------------------------------------------\n");
            str.append("Final result: ");
            str.append(m.getResult());
            str.append("\n");
        }
        area.setText(str.toString());
    }
}
